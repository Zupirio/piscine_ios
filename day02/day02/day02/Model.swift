//
//  Model.swift
//  day02
//
//  Created by Asahel RANGARIRA on 2018/10/03.
//  Copyright © 2018 Asahel RANGARIRA. All rights reserved.
//

import Foundation

struct Data {
    static let names :[(String, Int)] = [
        ("dravuth", 3008),
        ("praip", 3285),
        ("qoxass", 3185),
    ]
}
