//
//  ViewController.swift
//  day02
//
//  Created by Asahel RANGARIRA on 2018/10/03.
//  Copyright © 2018 Asahel RANGARIRA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{

    let list = ["Milk", "EGGs", "Bread"]
    @IBOutlet weak var tableView: UITableView!
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Data.names.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filmcell")
        cell?.textLabel?.text = Data.names[indexPath.row].0
        cell?.detailTextLabel?.text = String(Data.names[indexPath.row].1)
        
////        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "filmcel;")
////        cell.textLabel?.text = list[indexPath.row]
       return cell!
    }

//    let calendar = Calendar.current
//    let today = NSDate()
//    var minDateComponent : NSCalendar.Unit = [.year, .month, .day]
//
    
//    picker.minimumDate = minDate! as Date
//    picker.maximumDate =  maxDate! as Date
}
