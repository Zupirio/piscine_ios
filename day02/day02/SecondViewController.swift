//
//  SecondViewController.swift
//  day02
//
//  Created by Asahel RANGARIRA on 2018/10/03.
//  Copyright © 2018 Asahel RANGARIRA. All rights reserved.
//

import Foundation
import UIKit

class SecondView: UIViewController
{
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var dateLabel: UIDatePicker!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        <#code#>
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        <#code#>
//    }
    
}
