//
//  ViewController.swift
//  ex00
//
//  Created by Asahel RANGARIRA on 2018/10/01.
//  Copyright © 2018 Asahel RANGARIRA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloText: UILabel!
    @IBAction func clickButton(_ sender: UIButton) {
        helloText.text = "Goodbye World"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

