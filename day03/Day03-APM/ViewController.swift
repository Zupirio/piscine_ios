//
//  ViewController.swift
//  Day03-APM
//
//  Created by Khomotjo MODIPA on 2018/10/05.
//  Copyright © 2018 Khomotjo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    /* Data Source */
    let ImageArray:[String] = [
        "https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/45025340661_7b9f8f9402_k.jpg",
        "https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia22688.jpg",
        "https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/churning.jpeg",
        "https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/florence.jpeg",
        "https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/44343884472_899f141b0e_k.jpg",
        "https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/potw1008a.jpg"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /* Number of Views */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ImageArray.count
    }
    
    /* Populate Views */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier:  "imageCell", for: indexPath) as! imageViewCellClass
        
        if let url = URL(string: ImageArray[indexPath.row])
        {
            do {
                let imageData = try Data(contentsOf: url)
                imageCell.myImageView.image = UIImage(data: imageData)
            }catch let err {
                print("Error: \(err.localizedDescription)")
            }
        }
        //imageCell.myImageView.image = UIImage(named: ImageArray[indexPath.row])
        
         return imageCell
    }
}

