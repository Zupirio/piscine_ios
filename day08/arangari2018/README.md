# arangari2018

[![CI Status](https://img.shields.io/travis/Zupirio/arangari2018.svg?style=flat)](https://travis-ci.org/Zupirio/arangari2018)
[![Version](https://img.shields.io/cocoapods/v/arangari2018.svg?style=flat)](https://cocoapods.org/pods/arangari2018)
[![License](https://img.shields.io/cocoapods/l/arangari2018.svg?style=flat)](https://cocoapods.org/pods/arangari2018)
[![Platform](https://img.shields.io/cocoapods/p/arangari2018.svg?style=flat)](https://cocoapods.org/pods/arangari2018)

## Project Guideline

To run the project, clone the repo, and run `pod install` from the root directory first.

## Requirements
### MacOS
### CocoaPods

## Installation

arangari2018 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'arangari2018'
```

## Author

Zupirio, zupirio@gmail.com

## License

arangari2018 is available under the MIT license. See the LICENSE file for more info.
