

import Foundation
import MapKit

// Data Structs

struct Place {
    let name    : String
    let subtitle : String
    let lat     : Double
    let long    : Double
    let color   : UIColor
    
    static var places : [Place] = [
        Place(name: "WeThinkCode_", subtitle: "School", lat: -26.204881, long: 28.040324, color: UIColor.blue),
        Place(name: "BCX", subtitle: "ICT Company", lat: -25.832170, long: 28.175680, color: UIColor.red),
        Place(name: "Addis Ababa", subtitle: "Capital City", lat: 9.033140, long: 38.750080, color: UIColor.brown),
        Place(name: "Nelspruit", subtitle: "City", lat: -25.470100, long: 30.978081, color: UIColor.cyan)]
    
    
    
    // Mutables
    static var selectedPlace = Place.places[0]
    static var destinationPlace = Place.places[0]
}

class MapPin : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(title: String, subtitle: String, location: CLLocationCoordinate2D) {
        self.coordinate = location
        self.title = title
        self.subtitle = subtitle
    }
    
}


