//
//  FirstViewController.swift
//  Day005
//
//  Created by Felix Ntokozo THWALA on 2018/10/08.
//  Copyright © 2018 Felix Ntokozo THWALA. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FirstViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    var artworks: [Artwork] = []
    @IBOutlet weak var mapView: MKMapView!
    var locationManager = CLLocationManager()
    var lastUserLocation: MKUserLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set initial location in 42 Paris
        let initialLocation = CLLocationCoordinate2D(latitude: 48.8966, longitude: 2.3185)
        centerMapOnLocation(location: initialLocation)
        // show artwork on map
//        loadInitialData()
//        mapView.addAnnotations(artworks)
        let artwork = Artwork(title: "42",
                              locationName: "Ecole top stile",
                              discipline: "42 School",
                              coordinate: CLLocationCoordinate2D(latitude: 48.8966, longitude: 2.3185))
        mapView.addAnnotation(artwork)
        
        let Gauteng = Artwork(title: "Gauteng Legislature",
                              locationName: "Aluta Continua",
                              discipline: "Legislature",
                              coordinate: CLLocationCoordinate2D(latitude: -26.20441, longitude: 28.04122))
        mapView.addAnnotation(Gauteng)

        let Res = Artwork(title: "Res",
                              locationName: "Welcome Home",
                              discipline: "Residence",
                              coordinate: CLLocationCoordinate2D(latitude: -26.2056676, longitude: 28.0422466))
        mapView.addAnnotation(Res)
        
        
//        let Gauteng = MKPointAnnotation()
//        Gauteng.title = "Gauteng Legislature"
//        Gauteng.subtitle = "Aluta Continua"
//        Gauteng.coordinate = CLLocationCoordinate2D(latitude: -26.20441, longitude: 28.04122)
//        mapView.addAnnotation(Gauteng)
        
//        let Res = MKPointAnnotation()
//        Res.title = "Res"
//        Res.subtitle = "Welcome Home"
//        Res.coordinate = CLLocationCoordinate2D(latitude: -26.2056676, longitude: 28.0422466)
//        mapView.addAnnotation(Res)
        
        mapView.register(ArtworkMarkerView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)

        
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
//    let regionRadius: CLLocationDistance = 1000
//    func centerMapOnLocation(location: CLLocation) {
//        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
//        mapView.setRegion(coordinateRegion, animated: true)
//
//    }

    let regionRadius: CLLocationDistance = 1000
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeMapType(_ sender: UISegmentedControl) {
        mapView.mapType = MKMapType.init(rawValue: UInt(sender.selectedSegmentIndex)) ?? .standard
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Access the last object from locations to get perfect current location
        if let location = locations.last {
            let span = MKCoordinateSpanMake(0.00575, 0.00575)
            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
            let region = MKCoordinateRegionMake(myLocation, span)
            mapView.setRegion(region, animated: true)
        }
        self.mapView.showsUserLocation = true
        manager.stopUpdatingLocation()
        let fourtyTwo = CLLocationCoordinate2D(latitude: 48.8966, longitude: 2.3185)
        centerMapOnLocation(location: fourtyTwo)
    }

    @IBAction func geoLocation(_ sender: UIButton) {
//        locationManager.startUpdatingLocation()
        centerMapOnLocation(location: self.mapView.userLocation.coordinate)
    }

    func loadInitialData() {
        // 1
        guard let fileName = Bundle.main.path(forResource: "PublicArt", ofType: "json")
            else { return }
        let optionalData = try? Data(contentsOf: URL(fileURLWithPath: fileName))
        
        guard
            let data = optionalData,
            // 2
            let json = try? JSONSerialization.jsonObject(with: data),
            // 3
            let dictionary = json as? [String: Any],
            // 4
            let works = dictionary["data"] as? [[Any]]
            else { return }
        // 5
        let validWorks = works.compactMap { Artwork(json: $0) }
        artworks.append(contentsOf: validWorks)
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    var markerTintColor: UIColor  {
        switch title {
        case "Gauteng Legislature":
            return .green
        case "Res":
            return .cyan
        default:
            return .red
        }
    }

}

