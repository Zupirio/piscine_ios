//
//  main.swift
//  
//
//  Created by Asahel RANGARIRA on 2018/10/02.
//

import Foundation

enum Color : String {
    case black = "spades"
    case red = "hearts"
    case blue = "diamonds"
    case green = "clubs"
    
    static let allColors = [black, red, blue, green]
}
