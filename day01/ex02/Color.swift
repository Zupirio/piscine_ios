//
//  main.swift
//  
//
//  Created by Asahel RANGARIRA on 2018/10/02.
//

import Foundation

enum Color : String {
    case Hearts = "Red"
    case Spades = "Black" 
    case Diamonds = "White" 
    case Clubs = "Blue"
    
    static let allColors = [Hearts, Spades, Diamonds, Clubs]
}
