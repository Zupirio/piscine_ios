//
//  Deck.swift
//  
//
//  Created by Asahel Rangarira on 2018/10/03.
//
import Foundation

class Deck
{
    static let allSpades    : [Card] = Value.allValues.compactMap({Card(color:Color.Spades, value:$0)})
    static let allHearts    : [Card] = Value.allValues.compactMap({Card(color:Color.Hearts, value:$0)})
    static let allClubs     : [Card] = Value.allValues.compactMap({Card(color:Color.Clubs, value:$0)})
    static let allDiamonds  : [Card] = Value.allValues.compactMap({Card(color:Color.Diamonds, value:$0)})
    static let allCard     : [Card] = allSpades + allHearts + allClubs + allDiamonds
}

extension Array where Element:Card {
    mutating func ran() -> [Card]{
        var copyArray:[Card] = self
        self = []
        var i = 0;

        while i <= copyArray.count 
        {
            let rand = Int(arc4random_uniform(UInt32(copyArray.count)))
            self.append(copyArray[rand] as! Element)
            copyArray.remove(at: rand)
            i += 1
        }
        return self
    }
}