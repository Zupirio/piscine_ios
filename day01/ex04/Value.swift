import Foundation

enum Value : Int {
    case ace = 1, two, three, four, five, six, seven, eight, nine, ten, jack, queen, king
    
    static let allValues = [ace, two, three, four, five, six, seven, eight, nine, ten, jack, queen, king]
}
