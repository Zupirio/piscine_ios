import Foundation
import AppKit

class Deck : NSObject
{
    static let allSpades    : [Card] = Value.allValues.compactMap({Card(color:Color.Spades, value:$0)})
    static let allHearts    : [Card] = Value.allValues.compactMap({Card(color:Color.Hearts, value:$0)})
    static let allClubs     : [Card] = Value.allValues.compactMap({Card(color:Color.Clubs, value:$0)})
    static let allDiamonds  : [Card] = Value.allValues.compactMap({Card(color:Color.Diamonds, value:$0)})
    static let allCards      : [Card] = allSpades + allHearts + allClubs + allDiamonds
//dice1 = arc4random_uniform(6) + 1; 

    var cards:[Card]
    var ca:[Card]
    var discards:[Card] = []
    var outs:[Card] = []
    override var description: String{
        get {
            return "Cards\n \(cards)"
        }
    }
    
    init(deckIsSorted: Bool) {
        cards = Deck.allCards
        ca = []
        
        if !deckIsSorted {
            ca = cards.ran()
        }
    }
    
    func draw() -> Card {
        let drawnCard:Card = cards[0]
        cards.remove(at: 0)
        outs.append(drawnCard)
        return drawnCard
    }
    
    func fold(c: Card) {
        let ci:Int = outs.index(of: c)!
        discards.append(c)
        outs.remove(at: ci)
        
    }
}

extension Array where Element:Card {
    mutating func ran() -> [Card]{
        var copyArray:[Card] = self
        self = []
        var i = 0;

        while i <= copyArray.count 
        {
            let rand = Int(arc4random_uniform(UInt32(copyArray.count)))
            self.append(copyArray[rand] as! Element)
            copyArray.remove(at: rand)
            i += 1
        }
        return self
    }
}