import Foundation
import AppKit

class Card : NSObject{
    var color: Color
    var value: Value
    
    init(color: Color, value:Value){
        self.color = color
        self.value = value
        super.init()
    }

    override var description: String {
        return "\(color) of \(value)"
    }

    override func isEqual(_ object: Any?) -> Bool {
       if let obj = object as? Card{
            return (obj.color == self.color && obj.value == self.value)
        }
        return false
       }

       static func isEqual(color1: Color, color2: Color) -> Bool {
        if (color1.rawValue == color2.rawValue)
        {
            return true
        }
        else {
            return false
        }
    }

    // func ==(lsh: Card, rsh: Card) -> Bool
    // {
    //     return (lsh.color == rsh.color && lsh.value == rsh.value)
    // }
    
}