//
//  Card.swift
//
//
//  Created by Asahel RANGARIRA on 2018/10/02.
//
import Foundation
import AppKit

class Card : NSObject{
    var color: Color
    var value: Value
    
    init(color: Color, value:Value){
        self.color = color
        self.value = value
    }

    override var description: String {
        return "ColoR is \(color) and value is \(value)"
    }

    override func isEqual(_ object: Any?) -> Bool {
       if let obj = object as? Card{
            return (obj.color == self.color && obj.value == self.value)
        }
        return false
       }

     static func ==(lsh: Card, rsh: Card) -> Bool
    {
        return (lsh.color == rsh.color && lsh.value == rsh.value)
    }
}
