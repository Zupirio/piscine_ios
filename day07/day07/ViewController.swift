//
//  ViewController.swift
//  day07
//
//  Created by Asahel RANGARIRA on 2018/10/10.
//  Copyright © 2018 Asahel RANGARIRA. All rights reserved.
//

import UIKit
import RecastAI
import ForecastIO

class ViewController: UIViewController  {

    var bot: RecastAIClient?
    let client = DarkSkyClient(apiKey: "25a879afdf4e7cb4a53b405e92f7ee55")

    @IBAction func buttonLabel(_ sender: UIButton) {
        makeRequest()
    }
    @IBOutlet weak var userInput: UITextField!
    @IBOutlet weak var uiLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.bot = RecastAIClient(token: "ec805d272ac2ceb6ca3548c525d76de2")
        client.units = .si

    }
    
    func makeRequest()
    {
        self.bot?.textRequest(userInput.text!, successHandler: recastRequestDone, failureHandle: recastRequestNotDone)
    }
    

    func recastRequestDone(_ response: Response)
    {
        
        var myLat: Double = 0.0
        var myLng: Double = 0.0
        let location = response.get(entity: "location")
        print(location!)
        print(location!["lat"]!)
        print(location!["lng"]!)
        
        if let dlat = location!["lat"] as? Double
        {
            myLat = dlat
        }
        
        if  let dLng = location!["lng"] as? Double
        {
            myLng = dLng
        }
        
        client.getForecast(latitude: myLat, longitude: myLng)
        {
            result in
            switch result {
                case.success(let forecast):
                    print(forecast.0.currently!.summary!)
                    DispatchQueue.main.async {
                        self.uiLabel.text = forecast.0.currently!.summary!
                }
                
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
    }
    
    func recastRequestNotDone(_ err: Error)
    {
        print(err.localizedDescription)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

